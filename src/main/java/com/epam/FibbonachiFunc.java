package com.epam;


import java.util.Scanner;

/**
 * fibonacci class for finding fibonacci numbers, homework
 */
class FibbonachiFunc extends Application {
    public void Fibo() {
        int a = Application.a; //variable to be used as the previous member of the "equation"
        int b1 = Application.b1; //variable to be used as the previous member of the "equation"

        {
            System.out.println("\n your first 2 Fibbonachi numbers" + a + " & " + b1); //displays the first two sequences of Fibonacci members


            System.out.println("\n" +
                    "enter the number of repetitions  \n");

            Scanner scanner = new Scanner(System.in);
            int x = scanner.nextInt();//variable used for loop rep limit
            int[] arr = new int[x];

            int i = 0; //variable to be used to start the loop
            int sum = 0; //the variable used as the result of the function, and in subsequent "lives" of the cycle, as the previous member of the sequence
            System.out.println("Fibbonachi sequence: ");
            System.out.print(a + " " + b1);
            for (i = 0; i < x; i++) {
                sum = a + b1;
                a = b1; /** put the previous member of the sequence into the first variable to initiate the previous previous member of the sequence*/
                b1 = sum;/** put the previous member of the sequence in the second variable to initiate the previous member of the sequence*/
                System.out.print(" " + sum);
                arr[i] = sum;
            }
            /**according to the conditions of the problem, we find the percentage ratio*/
            float s = 0;
            float v = 0;
            for (i = 0; i < x; i++) {
                if (arr[i] % 2 == 0) {
                    s = i + 1;
                } else {
                    v = i + 1;
                }
            }

            System.out.println("\n percentage of odd and even Fibonacci numbers:\n" + "even " + (s / v) * 100 / 2 + " odd " + (100 - s / v * 100 / 2));

        }
    }
}